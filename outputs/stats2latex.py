#! /usr/bin/env python3

import sys

metrics = []

def toIgnore(model) :
  return "bt2" in model

def modelName(model) :
  if "tagger" in model.lower() :
    return "TAGGER"
  if "tagparser" in model.lower() :
    return "TAPARSER"
  if "parser" in model.lower() or "eager" in model.lower() :
    return "PARSER"
  if "bt1" in model.lower() :
    return "RL\_BT1"
  if "no" in model.lower() :
    return "RL\_NOBT"
  if "oracle" in model.lower() :
    return "SPV"
  print("ERROR : unknown model '%s'"%model, file=sys.stderr)
  exit(1)
  return "NOT FOUND"

def prettyInt(v) :
  if v == "0" :
    return "\\texttt{\_}"
  return "\\texttt{%s}"%",".join([v[::-1][i:i+3] for i in range(0,len(v),3)])[::-1]

def percent(v) :
  v = float(v)
  if v == 0 :
    return "\\texttt{\_}"
  return "\\texttt{%05.2f\%%}"%float(v)

targetValues = {"nbActions" : ("\#Actions",prettyInt),
                "nbErr" : ("\#Errs",prettyInt),
                "nbBack" : ("\#Backs",prettyInt),
                "backPrecision" : ("bPrec",percent),
                "backRecall" : ("bRec",percent),
                "nbRedoneCorrectCorrect" : ("C$\\rightarrow$C",percent),
                "nbRedoneErrErr" : ("E$\\rightarrow$E",percent),
                "nbRedoneCorrectErr" : ("C$\\rightarrow$E",percent),
                "nbRedoneErrCorrect" : ("E$\\rightarrow$C",percent)}
for t in targetValues :
  old = targetValues[t]
  targetValues[t] = ("\\texttt{%s}"%old[0],old[1])
asTab = []

modelsList = []
for line in open(sys.argv[1]) :
  line = line.strip()
  if len(line) == 0 :
    continue
  splited = line.split()
  splited[0] = splited[0].rstrip(".")
  if len(modelsList) == 0 :
    modelsList = splited[1:]
    asTab = [[""] + ["\\textbf{%s}"%modelName(m) for m in modelsList if not toIgnore(m)]]
    asTab += [[] for _ in range(len(targetValues))]
    continue
  if splited[0] in targetValues :
    asTab[1+list(targetValues.keys()).index(splited[0])] = [targetValues[splited[0]][0]]+list(map(targetValues[splited[0]][1], [splited[1+i] for i in range(len(modelsList)) if not toIgnore(modelsList[i])]))

toSort = list(map(modelName, [m for m in modelsList if not toIgnore(m)]))
inOrder = sorted(toSort)
newOrder = [toSort.index(v)+1 for v in inOrder]
for line in range(len(asTab)) :    
  asTab[line] = [asTab[line][0]] + [asTab[line][i] for i in newOrder]

print("""\\begin{table}[]
\centering
\\tabcolsep=0.8mm
\\begin{tabular}{|l|%s|}
\hline"""%("|".join("r"*(len(targetValues)))))
for line in asTab :
  print(" & ".join(line), "\\\\ \hline")
print("""\end{tabular}
\caption{}
\label{tab:my-table}
\end{table}""")

