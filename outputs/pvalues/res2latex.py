#! /usr/bin/env python3

import sys


def toIgnore(model) :
  return "bt2" in model

def modelName(model) :
  if "bt1" in model.lower() :
    return "RL\_BT1"
  if "no" in model.lower() :
    return "RL\_NOBT"
  if "oracle" in model.lower() :
    return "SL"
  print("ERROR %s not found"%model)
  exit(1)
  return "NOT FOUND"

def nicePValue(p) :
  if p == "_" :
    return "\_"
  return "%.3f"%p

for filename in sys.argv[1:] :
  metrics = []
  for line in open(filename) :
    line = line.strip()
    if len(line) == 0 or "------" in line :
      continue
    splited = line.split()
    if len(splited) == 1 :
      metrics.append([splited[0], []])
      continue
    metrics[-1][1].append([])
    metrics[-1][1][-1].append(splited[1])
    metrics[-1][1][-1].append(splited[2])
    metrics[-1][1][-1].append(splited[4])
    if len(splited) == 9 :
      metrics[-1][1][-1].append(splited[8].split('=')[-1])
    else :
      metrics[-1][1][-1].append("\_")
    
  asTab = [["\\textbf{Model}"] + ["\\textbf{%s}"%[m[0],"p"][i] for m in metrics for i in [0,1]]]
  models = {}
  for metric in metrics :
    for i in range(len(metric[1])) :
      model = metric[1][i][0]
      if model not in models :
        models[model] = []
      models[model].append([metric[1][i][1:]])
  for model in models :
    if toIgnore(model) :
      continue
    asTab.append([modelName(model)])
    asTab[-1] += [models[model][i][0][j] for i in range(len(metrics)) for j in [0,2]]
  
  print("""\\begin{table}
\centering
\\tabcolsep=0.8mm
\\begin{tabular}{|l|l|l|l|l|}
\hline""")
  for line in asTab :
    print(" & ".join(line), "\\\\ \hline")
  print("""\end{tabular}
\caption{%s on UD\_French-GSD}
\label{tab:res_%s}
\end{table}

"""%(filename.split('.')[0],filename.split('.')[0]))

