#! /usr/bin/env python3

import os
import sys

data = "../data/"
evalScript = "../conll18_ud_eval.py"
readTrace = "../readTrace.py"
pvalues = "pvalues/"
gold = "%sgold/"%pvalues

os.makedirs(gold, exist_ok=True)

groups = {}
splits = set()

for filename in os.listdir() :
  if len(filename) < 7 or filename[-7:] != ".conllu" or "bt2" in filename :
    continue
  splitNum = int(filename.split("_")[0])
  splits.add(splitNum)
  basename = "_".join((".".join(filename.split(".")[:-1])).split("_")[1:])
  group = basename.split("_")[0]
  if group not in groups :
    groups[group] = []
  if basename not in groups[group] :
    groups[group].append(basename)

refs = " ".join(["%sUD_French-GSD_%d/test.conllu"%(data, split) for split in splits])
os.system("cat %s > %s%s_corpus.conllu"%(refs, gold, "+".join(list(map(str,splits)))))

btTraces = []
for group in groups :
  print("group=%s"%group, file=sys.stderr)
  groupDir = "%s%s/"%(pvalues, group)
  for model in groups[group] :
    print("model=%s"%model, file=sys.stderr)
    modeloutputs = " ".join(["%d_%s.conllu"%(split, model) for split in splits])
    modelDir = "%s%s/"%(groupDir, model)
    os.makedirs("%s"%(modelDir), exist_ok=True)
    command = "cat %s > %s%s_corpus.conllu"%(modeloutputs, modelDir, "+".join(list(map(str,splits))))
    print(command, file=sys.stderr)
    os.system(command)

  btTraces.append(["%d_%s.trace"%(list(splits)[0], elem) for elem in groups[group] if "bt" in elem and "nobt" not in elem and "1" in elem][0])
  traces = ["%d_%s.trace"%(list(splits)[0], elem) for elem in groups[group]]
  for trace in traces :
    command = "%s %s --steps > steps_%s.txt"%(readTrace, trace, trace)
    print(command, file=sys.stderr)
    os.system(command)

command = "%s %s --stats > stats_bt.txt"%(readTrace, " ".join(btTraces))
print(command, file=sys.stderr)
os.system(command)

