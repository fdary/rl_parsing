#! /usr/bin/env bash

if [ "$#" -lt 2 ]
then
  >&2 echo "USAGE $0 modelName nbFolds(1..10) arguments..."
  exit 1
fi

NAME=$1
K=$(($2-1))

shift
shift
shift

for k in $(seq 0 $K)
do
  >&2 echo "Decoding of "bin/$NAME"_$k :"
  ./main.py decode model data/UD_French-GSD_$k/test.conllu "bin/$NAME"_$k > "bin/$NAME"_$k/predicted_test.conllu
  >&2 echo ""
  if [ "$k" -ne 0 ]
  then
    tail -n+2 "bin/$NAME"_$k/predicted_test.conllu >> total_test_predicted.conllu
    tail -n+2 data/UD_French-GSD_$k/test.conllu >> total_test_gold.conllu
  else
    cat "bin/$NAME"_$k/predicted_test.conllu > total_test_predicted.conllu
    cat data/UD_French-GSD_$k/test.conllu > total_test_gold.conllu
  fi
done

./conll18_ud_eval.py total_test_gold.conllu total_test_predicted.conllu

