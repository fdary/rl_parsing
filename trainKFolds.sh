#! /usr/bin/env bash

if [ "$#" -lt 3 ]
then
  >&2 echo "USAGE $0 oracle/rl modelName nbFolds(1..10) arguments..."
  exit 1
fi

MODE=$1
NAME=$2
K=$(($3-1))

shift
shift
shift

for k in $(seq 0 $K)
do
  >&2 echo "Training of "bin/$NAME"_$k :"
  ./main.py train $MODE data/UD_French-GSD_$k/train.conllu "bin/$NAME"_$k --dev data/UD_French-GSD_$k/dev.conllu $@
  >&2 echo ""
done

