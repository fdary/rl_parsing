import json
from readMCD import readMCD

################################################################################
class Dicts :
  unkToken = "__unknown__"
  nullToken = "__null__"
  noStackToken = "__nostack__"
  oobToken = "__oob__"
  noDepLeft = "__nodepleft__"
  noDepRight = "__nodepright__"
  noGov = "__nogov__"
  notSeen = "__notseen__"
  erased = "__erased__"

  def __init__(self) :
    self.dicts = {}
    self.foundLower = {}
    self.notFound = {}
    self.found = {}

  def addDict(self, name, d) :
    if name in self.dicts :
      raise(Exception(name+" already in dicts"))
    self.dicts[name] = d

  def readConllu(self, filename, colsSet=None, minCount=0, pretrained={}) :
    defaultMCD = "ID FORM LEMMA UPOS XPOS FEATS HEAD DEPREL DEPS MISC"
    col2index, index2col = readMCD(defaultMCD)

    targetColumns = []

    for line in open(filename, "r") :
      line = line.strip()
      if "# global.columns =" in line :
        mcd = line.split('=')[-1].strip()
        col2index, index2col = readMCD(mcd)
        continue
      if len(line) == 0 or line[0] == '#' :
        continue

      if len(targetColumns) == 0 :
        if colsSet is None :
          targetColumns = list(col2index.keys())
        else :
          targetColumns = list(colsSet)
        self.dicts = {col : {self.unkToken : (0,minCount), self.nullToken : (1,minCount), self.noStackToken : (2,minCount), self.oobToken : (3,minCount), self.noDepLeft : (4,minCount), self.noDepRight : (5,minCount), self.noGov : (6,minCount), self.notSeen : (7,minCount), self.erased : (8,minCount)} for col in targetColumns}

      splited = line.split('\t')
      for col in targetColumns :
        if col in pretrained :
          continue
        if col == "LETTER" :
          values = [letter for letter in splited[col2index["FORM"]]]
        else :
          values = [splited[col2index[col]]]
        for value in values :
          if value not in self.dicts[col] :
            self.dicts[col][value] = (len(self.dicts[col]),1)
          else :
            self.dicts[col][value] = (self.dicts[col][value][0],self.dicts[col][value][1]+1)

    for name in self.dicts :
      newDict = {}
      for value in self.dicts[name] :
        if self.dicts[name][value][1] >= minCount :
          newDict[value] = (len(newDict),self.dicts[name][value][1])
      self.dicts[name] = newDict

    for col in pretrained :
      self.readW2V(col, pretrained[col])

  def readW2V(self, col, file) :
    if col not in self.dicts :
      return
    embSize = None
    nbEmb = None
    for line in open(file, "r") :
      line = line.strip()
      if embSize is None :
        vals = list(map(int, line.split()))
        nbEmb = vals[0]
        embSize = vals[1]
        continue
      
      splited = line.split(' ')
      word = splited[0].replace("◌", " ")
      if word not in self.dicts[col] :
        self.dicts[col][word] = (len(self.dicts[col]), 1)

  def getSpecialIndexes(self, col) :
    res = set()
    for s in [
      Dicts.unkToken,
      Dicts.nullToken,
      Dicts.noStackToken,
      Dicts.oobToken,
      Dicts.noDepLeft,
      Dicts.noDepRight,
      Dicts.noGov,
      Dicts.notSeen,
      Dicts.erased,
    ] :
      res.add(self.get(col, s))
    return res

  def get(self, col, value) :
    if col not in self.dicts :
      raise Exception("Unknown dict name '%s' among %s"%(col, str(list(self.dicts.keys()))))
    if value in self.dicts[col] :
      if col not in self.found :
        self.found[col] = set()
      self.found[col].add(value)
      return self.dicts[col][value][0]
    if value.lower() in self.dicts[col] :
      if col not in self.foundLower :
        self.foundLower[col] = set()
      self.foundLower[col].add(value)
      return self.dicts[col][value.lower()][0]

    if col not in self.notFound :
      self.notFound[col] = set()
    self.notFound[col].add(value)
    return self.dicts[col][self.unkToken][0]

  def getElementsOf(self, col) :
    if col not in self.dicts :
      raise Exception("Unknown dict name %s"%col)
    return self.dicts[col].keys()

  def save(self, target) :
    json.dump(self.dicts, open(target, "w"))

  def load(self, target) :
    self.dicts = json.load(open(target, "r"))

  def printStats(self, output) :
    for col in self.dicts :
      total = 0
      if col in self.found :
        total += len(self.found[col])
      if col in self.notFound :
        total += len(self.notFound[col])
      if col in self.foundLower :
        total += len(self.foundLower[col])
      if total == 0 :
        continue
      print(col, file=output)
      if col in self.found :
        print("Found : %.2f%%"%(100.0*len(self.found[col])/total), file=output)
      if col in self.foundLower :
        print("Found Lower : %.2f%%"%(100.0*len(self.foundLower[col])/total), file=output)
      if col in self.notFound :
        print("Not found : %.2f%%"%(100.0*len(self.notFound[col])/total), file=output)

  def resetStats(self) :
    self.found = {}
    self.notFound = {}
    self.foundLower = {}
################################################################################

